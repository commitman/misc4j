package tk.labyrinth.misc4j.util;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.LongSupplier;

/**
 * Supports:<br>
 * - {@link TimeUnit#SECONDS}<br>
 * - {@link TimeUnit#MILLISECONDS}<br>
 * - {@link TimeUnit#MICROSECONDS}<br>
 * - {@link TimeUnit#NANOSECONDS}<br>
 * Does not support:<br>
 * - {@link TimeUnit#DAYS}<br>
 * - {@link TimeUnit#HOURS}<br>
 * - {@link TimeUnit#MINUTES}<br>
 */
public class Timestamper {

	private static final Timestamper millisTimestamper = of(TimeUnit.MILLISECONDS);

	private final AtomicLong timestamp = new AtomicLong(0);

	private final LongSupplier nowSupplier;

	public Timestamper(LongSupplier nowSupplier) {
		this.nowSupplier = nowSupplier;
	}

	/**
	 * Returns unique value which is greater or equal to current time
	 * calculated with precision of this timestamper's {@link TimeUnit}.
	 */
	public long get() {
		return timestamp.updateAndGet(timestamp -> {
			long now = nowSupplier.getAsLong();
			return now > timestamp ? now : timestamp + 1;
		});
	}

	public static Timestamper from(@Nonnull TimeUnit timeUnit, long timestamp) {
		Timestamper result = of(timeUnit);
		result.timestamp.set(timestamp);
		return result;
	}

	public static long getMillis() {
		return millisTimestamper.get();
	}

	public static Timestamper of(@Nonnull TimeUnit timeUnit) {
		Objects.requireNonNull(timeUnit, "timeUnit");
		LongSupplier nowSupplier;
		switch (timeUnit) {
			case MICROSECONDS:
				nowSupplier = () -> System.nanoTime() / 1000;
				break;
			case MILLISECONDS:
				nowSupplier = System::currentTimeMillis;
				break;
			case NANOSECONDS:
				nowSupplier = System::nanoTime;
				break;
			case SECONDS:
				nowSupplier = () -> System.currentTimeMillis() / 1000;
				break;
			default:
				throw new IllegalArgumentException(timeUnit.name());
		}
		return new Timestamper(nowSupplier);
	}
}
