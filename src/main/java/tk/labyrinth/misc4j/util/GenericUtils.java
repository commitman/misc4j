package tk.labyrinth.misc4j.util;

public class GenericUtils {

	/**
	 * Casts provided Object to any expected T with unchecked warning suppressed.
	 *
	 * @param object nullable
	 *
	 * @return object
	 */
	@SuppressWarnings("unchecked")
	public static <T> T cast(Object object) {
		return (T) object;
	}
}
