package tk.labyrinth.misc4j.collectoin;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

public class IterableUtils {

	public static <E> void forEachWithIndex(@Nonnull Iterable<? extends E> iterable, @Nonnull BiConsumer<Integer, ? super E> consumer) {
		IteratorUtils.forEachWithIndex(iterable.iterator(), consumer);
	}
}
