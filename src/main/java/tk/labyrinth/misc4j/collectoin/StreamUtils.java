package tk.labyrinth.misc4j.collectoin;

import tk.labyrinth.misc4j.exception.NotImplementedException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class StreamUtils {

	/**
	 * Returns a product of sequential concatenation of provided streams.
	 * This is a vararg alternative of {@link Stream#concat}.<br>
	 * <br>
	 * Note that this method does additional null-check before invoking {@link Stream#flatMap} for each element.
	 * This may result in a NPE thrown later when returned stream is terminated.<br>
	 *
	 * @param streams non-null
	 *
	 * @return non-null
	 *
	 * @throws NullPointerException for null vararg or null element
	 */
	@SafeVarargs
	public static <T> Stream<T> concat(Stream<? extends T>... streams) {
		return Stream.of(streams).flatMap(stream -> {
			Objects.requireNonNull(stream);
			return stream;
		});
	}

	public static <T> Stream<T> from(Enumeration<T> enumeration) {
		Objects.requireNonNull(enumeration, "enumeration");
		return from(IteratorUtils.from(enumeration));
	}

	public static <T> Stream<T> from(Iterator<T> iterator) {
		Objects.requireNonNull(iterator, "iterator");
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
	}

	@Nullable
	public static <T> Stream<T> fromNullable(@Nullable Enumeration<T> enumeration) {
		return enumeration != null ? from(enumeration) : null;
	}

	@Nullable
	public static <T> Stream<T> fromNullable(@Nullable Iterator<T> iterator) {
		return iterator != null ? from(iterator) : null;
	}

	@Nullable
	public static <T> Stream<T> fromOrDefault(@Nullable Collection<T> collection, @Nullable Stream<T> defaultValue) {
		return collection != null ? collection.stream() : defaultValue;
	}

	@Nonnull
	public static <T> Stream<T> fromOrEmpty(@Nullable Collection<T> collection) {
		return collection != null ? collection.stream() : Stream.empty();
	}

	@Nullable
	public static <T> Stream<T> fromOrNull(@Nullable Collection<T> collection) {
		return fromOrDefault(collection, null);
	}

	public static <T> T getOnly(@Nullable Stream<T> stream, boolean canBeNull, boolean canBeEmpty, boolean canHaveMultiple) {
		T result;
		if (stream != null) {
			List<T> list = stream.collect(Collectors.toList());
			if (!list.isEmpty()) {
				if (list.size() == 1) {
//					result = list.get()
				}
				throw new NotImplementedException();
			} else if (canBeEmpty) {
				result = null;
			} else {
				throw new IllegalArgumentException("Required to be non-empty: " + list);
			}
		} else if (canBeNull) {
			result = null;
		} else {
			throw new IllegalArgumentException("Required to be non-null");
		}
		return result;
	}
}
