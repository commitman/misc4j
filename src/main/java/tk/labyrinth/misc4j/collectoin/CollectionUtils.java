package tk.labyrinth.misc4j.collectoin;

import java.util.Arrays;

public class CollectionUtils {

	public static <T> boolean in(T element, T... elements) {
		return Arrays.asList(elements).contains(element);
	}
}
