package tk.labyrinth.misc4j.collectoin;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class IteratorUtils {

	public static <E> void forEachWithIndex(@Nonnull Iterator<? extends E> iterator, @Nonnull BiConsumer<Integer, ? super E> consumer) {
		int index = 0;
		while (iterator.hasNext()) {
			consumer.accept(index++, iterator.next());
		}
	}

	public static <E> Iterator<E> from(Enumeration<E> enumeration) {
		Objects.requireNonNull(enumeration, "enumeration");
		return new Iterator<E>() {
			@Override
			public boolean hasNext() {
				return enumeration.hasMoreElements();
			}

			@Override
			public E next() {
				return enumeration.nextElement();
			}
		};
	}

	@Nullable
	public static <E> Iterator<E> fromNullable(@Nullable Enumeration<E> enumeration) {
		return enumeration != null ? from(enumeration) : null;
	}
}
