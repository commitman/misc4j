package tk.labyrinth.misc4j.collectoin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CollectorUtils {

	/**
	 * @param <E> Element
	 *
	 * @return
	 */
	public static <E> Collector<E, ?, E> findOnly(boolean allowEmpty, boolean allowNull) {
		return Collectors.collectingAndThen(Collectors.toList(), list -> {
			if (list.size() > 1) {
				throw new IllegalArgumentException("Require only element: " + list);
			}
			E result;
			if (!list.isEmpty()) {
				result = list.get(0);
				if (result == null && !allowNull) {
					throw new IllegalArgumentException("Require non-null element: " + list);
				}
			} else {
				if (!allowEmpty) {
					throw new IllegalArgumentException("Require non-empty");
				} else {
					result = null;
				}
			}
			return result;
		});
	}

	/**
	 * Unlike {@link Collectors#toList()} this Collector guarantees to use {@link ArrayList}.<br>
	 *
	 * @param <E> Element
	 *
	 * @return non-null
	 */
	public static <E> Collector<E, ?, List<E>> toArrayList() {
		return Collectors.toCollection(ArrayList::new);
	}
}
