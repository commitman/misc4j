package tk.labyrinth.misc4j.collectoin;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListUtils {

	@Nullable
	public static <E> List<E> collect(@Nullable Stream<E> stream) {
		return stream != null ? stream.collect(Collectors.toList()) : null;
	}
}
