package tk.labyrinth.misc4j.codestyle;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Package level annotation to mark all fields, method return values and parameters of all classes within this package as {@link Nonnull @Nonnull}.
 * You may annotate element or its declaring class (or its declaring class, etc.) with {@link Nullable @Nullable} to override this mark.<br>
 * <br>
 * This annotation is designed to enforce mass null checks, specified in <a href="https://jcp.org/en/jsr/detail?id=305">JSR 305<a>.<br>
 * See implementation of JSR 305: <a href="https://mvnrepository.com/artifact/com.google.code.findbugs/jsr305">com.google.code.findbugs:jsr305<a>.<br>
 * See Spring alternative (which does not mark fields): <a href="https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/lang/NonNullApi.html">@NonNullApi<a>.<br>
 * <br>
 * NOTE: This annotation is package level so it must be placed inside <a href="https://docs.oracle.com/javase/specs/jls/se7/html/jls-7.html#jls-7.4.1">package-info.java<a>.<br>
 *
 * @author Commitman
 * @version 1.0.0
 */
@Documented
@Nonnull
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PACKAGE)
@TypeQualifierDefault({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface PackageLevelNonnull {
	// empty
}
