package tk.labyrinth.misc4j.exception;

/**
 * @author Commitman
 * @version 1.0.0
 */
public final class NotImplementedException extends RuntimeException {

	public NotImplementedException() {
		super(detectMethod());
	}

	public NotImplementedException(String message) {
		super(message);
	}

	private static String detectMethod() {
		return Thread.currentThread().getStackTrace()[3].toString();
	}
}
