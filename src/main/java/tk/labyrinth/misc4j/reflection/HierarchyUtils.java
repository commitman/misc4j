package tk.labyrinth.misc4j.reflection;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class HierarchyUtils {

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 */
	private static Stream<Class<?>> getRawClassStream(Class<?> type) {
		Class<?> superType = type.getSuperclass();
		return Stream.concat(
				Stream.of(type),
				Stream.concat(
						superType != null ? Stream.of(superType) : Stream.empty(),
						Arrays.stream(type.getInterfaces())
				).flatMap(HierarchyUtils::getRawClassStream)
		).distinct();
	}

	/**
	 * Returns Stream of unique methods declared within hierarchy of provided type.<br>
	 * Result will always contain {@link Object} methods.<br>
	 *
	 * @param type non-null
	 *
	 * @return non-empty
	 */
	public static Stream<Method> getDeclaredMethods(Class<?> type) {
		return getTypes(type).flatMap(innerType -> Stream.of(innerType.getDeclaredMethods()));
	}

	/**
	 * Returns a chain of Types representing shortest way from subtype to supertype.
	 *
	 * @param subtype   non-null
	 * @param supertype non-null
	 * @param <T>       parameter to enforce relation between subtype and supertype
	 *
	 * @return non-null chain with at least one element
	 *
	 * @throws IllegalArgumentException if supertype is not assignable from subtype
	 */
	public static <T> TypeChain getTypeChain(Class<? extends T> subtype, Class<? super T> supertype) {
		return getTypeChain((Type) subtype, supertype);
	}

	/**
	 * Returns a chain of Types representing shortest way from subtype to supertype.
	 *
	 * @param subtype   non-null
	 * @param supertype non-null
	 *
	 * @return non-null chain with at least one element
	 *
	 * @throws IllegalArgumentException if supertype is not assignable from subtype
	 */
	public static TypeChain getTypeChain(Type subtype, Class<?> supertype) {
		Class<?> subclass = TypeUtils.getClass(subtype);
		if (!supertype.isAssignableFrom(subclass)) {
			throw new IllegalArgumentException("Wrong relationship: subtype = " + subtype + ", supertype = " + supertype);
		}
		List<TypeChain> chains = new ArrayList<>();
		chains.add(TypeChain.of(subtype));
		TypeChain result = null;
		while (result == null) {
			TypeChain chain = chains.remove(0);
			Class<?> innerSubclass = TypeUtils.getClass(chain.getLast());
			if (innerSubclass == supertype) {
				result = chain;
			} else {
				if (supertype.isInterface()) {
					Stream.of(innerSubclass.getGenericInterfaces())
							.forEach(genericInterface -> chains.add(chain.append(genericInterface)));
				}
				{
					Type genericSuperclass = innerSubclass.getGenericSuperclass();
					if (genericSuperclass != null) {
						chains.add(chain.append(genericSuperclass));
					}
				}
			}
		}
		return result;
	}

	/**
	 * Returns Stream of classes and interfaces that are extended or implemented by provided type,<br>
	 * i.e. for which {@link Class#isAssignableFrom(Class) Class.isAssignableFrom(type)} returns true.<br>
	 * Result will always contain {@link Object Object.class}.<br>
	 *
	 * @param type non-null
	 *
	 * @return non-empty
	 */
	public static Stream<Class<?>> getTypes(Class<?> type) {
		return getRawClassStream(Objects.requireNonNull(type, "type")).distinct();
	}
}
