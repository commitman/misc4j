package tk.labyrinth.misc4j.reflection;

import lombok.Value;
import lombok.experimental.Wither;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/**
 * FIXME: Not sure we need this Class.
 */
@Value
public class ResolvedParameterEntry {

	Class<?> declaringType;

	int index;

	TypeVariable<?> placeholder;

	@Wither
	Type value;
}
