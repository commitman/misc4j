package tk.labyrinth.misc4j.reflection;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @version 1.0.0
 */
public class TypeUtils {

	/**
	 * @param type ParameterizedType instance
	 */
	public static ParameterizedType asParameterizedType(Type type) {
		Objects.requireNonNull(type, "type");
		//
		if (type instanceof ParameterizedType) {
			return (ParameterizedType) type;
		} else {
			throw new IllegalArgumentException("Not ParameterizedType: " + type.getTypeName());
		}
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 */
	public static Class<?> getActualClass(Type type) {
		Objects.requireNonNull(type, "type");
		//
		return getClass(getActualType(type));
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 */
	public static Type getActualType(Type type) {
		Objects.requireNonNull(type, "type");
		//
		Type result;
		if (isOptional(type)) {
			result = getFirstArgumentType(type);
		} else {
			result = type;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Object getActualValue(Object value) {
		return value instanceof Optional ? ((Optional) value).orElse(null) : value;
	}

	public static Class<?> getArgumentClass(Type type, int index) {
		return getClass(getArgumentType(type, index));
	}

	public static Type getArgumentType(ParameterizedType type, int index) {
		return getArgumentTypes(type).skip(index).findFirst().orElse(null);
	}

	public static Type getArgumentType(Type type, int index) {
		return getArgumentTypes(type).skip(index).findFirst().orElse(null);
	}

	public static Stream<Type> getArgumentTypes(ParameterizedType type) {
		Objects.requireNonNull(type, "type");
		//
		return Stream.of(type.getActualTypeArguments());
	}

	public static Stream<Type> getArgumentTypes(Type type) {
		return getArgumentTypes(asParameterizedType(type));
	}

	/**
	 * @param type non-null
	 *
	 * @return non-null
	 */
	public static Class<?> getClass(Type type) {
		Objects.requireNonNull(type, "type");
		//
		Class<?> result;
		if (type instanceof Class) {
			result = (Class<?>) type;
		} else if (type instanceof ParameterizedType) {
			result = (Class<?>) ((ParameterizedType) type).getRawType();
		} else {
			throw new UnsupportedOperationException(type.toString());
		}
		return result;
	}

	public static Class<?> getFirstArgumentClass(Type type) {
		return getArgumentClass(type, 0);
	}

	public static Type getFirstArgumentType(ParameterizedType type) {
		return getArgumentType(type, 0);
	}

	public static Type getFirstArgumentType(Type type) {
		return getArgumentType(type, 0);
	}

	public static Type getSecondArgumentType(ParameterizedType type) {
		return getArgumentType(type, 1);
	}

	public static Type getSecondArgumentType(Type type) {
		return getArgumentType(type, 1);
	}

	/**
	 * @param type non-null
	 */
	public static boolean isOptional(Type type) {
		Objects.requireNonNull(type, "type");
		//
		return getClass(type) == Optional.class;
	}
}
