package tk.labyrinth.misc4j.reflection;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EqualsAndHashCode
@ToString
public class TypeChain {

	private final List<Type> types;

	public TypeChain(Stream<Type> types) {
		this.types = types.collect(Collectors.toList());
	}

	public TypeChain append(Type type) {
		Objects.requireNonNull(type, "type");
		//
		return new TypeChain(Stream.concat(types.stream(), Stream.of(type)));
	}

	public TypeChain append(TypeChain typeChain) {
		Objects.requireNonNull(typeChain, "typeChain");
		//
		return new TypeChain(Stream.concat(types.stream(), typeChain.types.stream()));
	}

	public Type getLast() {
		return types.get(types.size() - 1);
	}

	public Stream<Type> getTypes() {
		return types.stream();
	}

	public TypeChain inverse() {
		List<Type> types = new ArrayList<>(this.types);
		Collections.reverse(types);
		return new TypeChain(types.stream());
	}

	public TypeChain prepend(Type type) {
		Objects.requireNonNull(type, "type");
		//
		return new TypeChain(Stream.concat(Stream.of(type), types.stream()));
	}

	public TypeChain prepend(TypeChain typeChain) {
		Objects.requireNonNull(typeChain, "typeChain");
		//
		return new TypeChain(Stream.concat(typeChain.types.stream(), types.stream()));
	}

	public static TypeChain of(Type type) {
		Objects.requireNonNull(type, "type");
		//
		return new TypeChain(Stream.of(type));
	}
}
