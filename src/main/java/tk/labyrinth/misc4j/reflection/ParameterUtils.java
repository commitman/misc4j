package tk.labyrinth.misc4j.reflection;

import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParameterUtils {

	static Stream<ResolvedParameterEntry> resolveParameters(Type type) {
		ParameterizedType parameterizedType = TypeUtils.asParameterizedType(type);
		Class<?> cl = TypeUtils.getClass(parameterizedType.getRawType());
		List<TypeVariable<?>> declared = Arrays.asList(cl.getTypeParameters());
		List<Type> actual = Arrays.asList(parameterizedType.getActualTypeArguments());
		return IntStream.range(0, declared.size()).mapToObj(index -> new ResolvedParameterEntry(
				cl, index, declared.get(index), actual.get(index)));
	}

	/**
	 * Casts provided Object to any expected T with unchecked warning suppressed.
	 *
	 * @param object nullable
	 *
	 * @return object
	 */
	@SuppressWarnings("unchecked")
	public static <T> T cast(Object object) {
		return (T) object;
	}

	/**
	 * For Class or Interface.
	 */
	public static <T> Stream<Type> getActualParameters(Class<? extends T> actualType, Class<? super T> declaringType) {
		return rawGetActualParameters((Type) actualType, declaringType);
	}

	/**
	 * For Constructor or Method arguments.
	 */
	public static <T> Stream<Type> getActualParameters(Executable executable, int index, Class<? super T> declaringType) {
		return rawGetActualParameters(executable.getGenericParameterTypes()[index], declaringType);
	}

	/**
	 * For Field.
	 */
	public static Stream<Type> getActualParameters(Field field, Class<?> declaringType) {
		return rawGetActualParameters(field.getGenericType(), declaringType);
	}

	/**
	 * For Method return type.
	 */
	public static <T> Stream<Type> getActualParameters(Method method, Class<? super T> declaringType) {
		return rawGetActualParameters(method.getGenericReturnType(), declaringType);
	}

	/**
	 * Named differently to avoid shadowing Class-type method.
	 */
	public static Stream<Type> rawGetActualParameters(Type actualType, Class<?> declaringType) {
		TypeChain chain = HierarchyUtils.getTypeChain(actualType, declaringType);
		Map<TypeVariable<?>, Type> types = chain.getTypes()
				.filter(type -> type instanceof ParameterizedType)
				.flatMap(ParameterUtils::resolveParameters)
				.collect(Collectors.toMap(ResolvedParameterEntry::getPlaceholder, ResolvedParameterEntry::getValue));
		List<Type> resultTypes = ParameterUtils.resolveParameters(chain.getLast())
				.map(ResolvedParameterEntry::getValue)
				.collect(Collectors.toList());
		return resultTypes.stream().map(type -> {
			Type resultType = type;
			while (resultType instanceof TypeVariable) {
				Type nextType = types.get(resultType);
				if (nextType != null) {
					resultType = nextType;
				} else {
					break;
				}
			}
			return resultType;
		});
	}
}
