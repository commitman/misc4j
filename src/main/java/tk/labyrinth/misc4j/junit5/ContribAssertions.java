package tk.labyrinth.misc4j.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

public class ContribAssertions {

	public static void assertInstanceOf(Class<?> type, Object object) {
		if (!type.isInstance(object)) {
			Assertions.fail("Instance of expected:" +
					"\nType: " + type.getName() +
					"\nObject: " + object);
		}
	}

	public static void assertThrows(Class<? extends Throwable> expectedType, String expectedMessage, Executable executable) {
		Throwable actual = null;
		try {
			executable.execute();
		} catch (Throwable t) {
			actual = t;
		} finally {
			Assertions.assertNotNull(actual);
			Assertions.assertEquals(expectedType, actual.getClass());
			Assertions.assertEquals(expectedMessage, actual.getMessage());
		}
	}
}
