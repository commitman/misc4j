package tk.labyrinth.misc4j.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

import java.util.function.Supplier;

public class ExAssertions {

	/**
	 * @see Assertions#assertEquals(Object, Object)
	 */
	public static Executable assertEquals(Object expected, Object actual) {
		return () -> Assertions.assertEquals(expected, actual);
	}

	/**
	 * @see Assertions#assertNotNull(Object)
	 */
	public static Executable assertNotNull(Object actual) {
		return () -> Assertions.assertNotNull(actual);
	}

	/**
	 * @see Assertions#assertNotNull(Object, String)
	 */
	public static Executable assertNotNull(Object actual, String message) {
		return () -> Assertions.assertNotNull(actual, message);
	}

	/**
	 * @see Assertions#assertNotNull(Object, Supplier)
	 */
	public static Executable assertNotNull(Object actual, Supplier<String> messageSupplier) {
		return () -> Assertions.assertNotNull(actual, messageSupplier);
	}

	/**
	 * @see Assertions#assertThrows(Class, Executable)
	 */
	public static <T extends Throwable> Executable assertThrows(Class<T> expectedType, Executable executable) {
		return () -> Assertions.assertThrows(expectedType, executable);
	}

	/**
	 * @see Assertions#assertThrows(Class, Executable, String)
	 */
	public static <T extends Throwable> Executable assertThrows(Class<T> expectedType, Executable executable, String message) {
		return () -> Assertions.assertThrows(expectedType, executable, message);
	}

	/**
	 * @see Assertions#assertThrows(Class, Executable, Supplier)
	 */
	public static <T extends Throwable> Executable assertThrows(Class<T> expectedType, Executable executable, Supplier<String> messageSupplier) {
		return () -> Assertions.assertThrows(expectedType, executable, messageSupplier);
	}
}
