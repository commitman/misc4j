package tk.labyrinth.misc4j.net;

import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class UrlUtils {

	public static URL from(URI uri) {
		try {
			return uri.toURL();
		} catch (MalformedURLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Nullable
	public static URL fromNullable(@Nullable URI uri) {
		return uri != null ? from(uri) : null;
	}
}
