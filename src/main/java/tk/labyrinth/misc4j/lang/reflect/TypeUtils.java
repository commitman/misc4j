package tk.labyrinth.misc4j.lang.reflect;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.stream.Stream;

public class TypeUtils {

	public static int getParameterCountOrZero(Type type) {
		return (int) getParametersOrEmpty(type).count();
	}

	public static Stream<Type> getParametersOrEmpty(Type type) {
		Stream<Type> result;
		{
			if (type instanceof Class) {
				result = Stream.of(((Class<?>) type).getTypeParameters());
			} else if (type instanceof ParameterizedType) {
				result = Stream.of(((ParameterizedType) type).getActualTypeArguments());
			} else {
				result = Stream.empty();
			}
		}
		return result;
	}
}
