package tk.labyrinth.misc4j.lang.reflect;

import java.lang.reflect.Type;
import java.util.stream.Stream;

public class ParameterUtils {

	public static Stream<Type> getActualParameters(Type actualType, Class<?> declaringType) {
		return tk.labyrinth.misc4j.lang.struct.ParameterUtils.getActualParameters(
				new TypeTypeDescriptor(actualType), new TypeTypeDescriptor(declaringType));
	}
}
