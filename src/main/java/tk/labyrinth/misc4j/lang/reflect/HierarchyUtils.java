package tk.labyrinth.misc4j.lang.reflect;

import tk.labyrinth.misc4j.lang.struct.TypeDescriptor;

import java.lang.reflect.Type;
import java.util.stream.Stream;

public class HierarchyUtils {

	public static Stream<Type> getTypeChain(Type subtype, Type supertype) {
		return tk.labyrinth.misc4j.lang.struct.HierarchyUtils.getChain(new TypeTypeDescriptor(subtype), new TypeTypeDescriptor(supertype))
				.getDescriptors().map(TypeDescriptor::getElement);
	}
}
