package tk.labyrinth.misc4j.lang.reflect;

import lombok.Value;
import tk.labyrinth.misc4j.lang.struct.TypeDescriptor;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.stream.Stream;

@Value
public class TypeTypeDescriptor implements TypeDescriptor<Type, TypeTypeDescriptor> {

	private final Type element;

	@Override
	public Stream<TypeDescriptor<Type, TypeTypeDescriptor>> getDirectSuperinterfaces() {
		return Stream.of(tk.labyrinth.misc4j.reflection.TypeUtils.getClass(element).getGenericInterfaces()).map(TypeTypeDescriptor::new);
	}

	@Override
	public Stream<TypeDescriptor<Type, TypeTypeDescriptor>> getParameters() {
		return TypeUtils.getParametersOrEmpty(element).map(TypeTypeDescriptor::new);
	}

	@Override
	public TypeDescriptor<Type, TypeTypeDescriptor> getRaw() {
		return new TypeTypeDescriptor(tk.labyrinth.misc4j.reflection.TypeUtils.getClass(element));
	}

	@Nullable
	@Override
	public TypeDescriptor<Type, TypeTypeDescriptor> getSuperclass() {
		Class<?> cl = tk.labyrinth.misc4j.reflection.TypeUtils.getClass(element).getSuperclass();
		return cl != null ? new TypeTypeDescriptor(cl) : null;
	}

	@Override
	public boolean isSubtype(TypeDescriptor<Type, ?> descriptor) {
		return tk.labyrinth.misc4j.reflection.TypeUtils.getClass(descriptor.getElement()).isAssignableFrom(
				tk.labyrinth.misc4j.reflection.TypeUtils.getClass(element));
	}

	@Override
	public boolean isVariable() {
		return element instanceof TypeVariable;
	}
}
