package tk.labyrinth.misc4j.lang.struct;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParameterUtils {

	static <E, D extends TypeDescriptor<E, D>> Stream<ParameterMapping<E, D>> resolveParameters(TypeDescriptor<E, D> parameterizedDescriptor) {
		TypeDescriptor<E, D> rawDescriptor = parameterizedDescriptor.getRaw();
		return IntStream.range(0, parameterizedDescriptor.getParameterCount()).mapToObj(index -> new ParameterMapping<>(
				rawDescriptor.getParameter(index),
				parameterizedDescriptor.getParameter(index)
		));
	}

	public static <E, D extends TypeDescriptor<E, D>> Stream<E> getActualParameters(TypeDescriptor<E, D> actualDescriptor, TypeDescriptor<E, D> declaringDescriptor) {
		TypeDescriptorChain<E, D> chain = HierarchyUtils.getChain(actualDescriptor, declaringDescriptor);
		Map<TypeDescriptor<E, D>, TypeDescriptor<E, D>> parameterMappings = chain.getDescriptors()
				.filter(TypeDescriptor::hasParameters)
				.flatMap(ParameterUtils::resolveParameters)
				.collect(Collectors.toMap(ParameterMapping::getFrom, ParameterMapping::getTo));
		return declaringDescriptor.getParameters()
				.map(descriptor -> {
					TypeDescriptor<E, D> resultDescriptor = descriptor;
					while (resultDescriptor.isVariable()) {
						TypeDescriptor<E, D> nextDescriptor = parameterMappings.get(resultDescriptor);
						if (nextDescriptor != null && !Objects.equals(nextDescriptor, resultDescriptor)) {
							resultDescriptor = nextDescriptor;
						} else {
							break;
						}
					}
					return resultDescriptor;
				}).map(TypeDescriptor::getElement);
	}
}
