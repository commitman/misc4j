package tk.labyrinth.misc4j.function;

import java.util.function.Consumer;

public class Consumers {

	/**
	 * @return non-null
	 */
	public static <T> Consumer<T> noOp() {
		return value -> {
			// no-op
		};
	}
}
