package tk.labyrinth.misc4j.stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j.junit5.ExAssertions;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StreamUtilsTest {

	@Test
	void testConcat() {
		Assertions.assertAll(
				ExAssertions.assertEquals(
						Collections.emptyList(),
						StreamUtils.concat().collect(Collectors.toList())),
				ExAssertions.assertEquals(
						Collections.singletonList(1),
						StreamUtils.concat(Stream.of(1)).collect(Collectors.toList())),
				ExAssertions.assertEquals(
						Arrays.asList(1, 2),
						StreamUtils.concat(Stream.of(1), Stream.of(2)).collect(Collectors.toList())),
				ExAssertions.assertEquals(
						Arrays.asList(1, 2, 3),
						StreamUtils.concat(Stream.of(1), Stream.of(2), Stream.of(3)).collect(Collectors.toList()))
		);
		{
			// Will throw NPE for null vararg.
			ExAssertions.assertThrows(
					NullPointerException.class,
					() -> StreamUtils.concat((Stream<?>[]) null), "");
			// Will not throw null-element NPE if not terminated.
			ExAssertions.assertNotNull(StreamUtils.concat(null, Stream.of(1), null));
			// Will throw null-element NPE when terminated.
			ExAssertions.assertThrows(
					NullPointerException.class,
					() -> StreamUtils.concat(null, Stream.of(1), null).collect(Collectors.toList()),
					"");
		}
	}
}
