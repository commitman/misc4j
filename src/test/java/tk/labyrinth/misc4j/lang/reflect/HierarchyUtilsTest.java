package tk.labyrinth.misc4j.lang.reflect;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j.reflection.DiamondGenerics;
import tk.labyrinth.misc4j.reflection.TypeUtils;

import java.lang.reflect.Type;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class HierarchyUtilsTest {

	private List<Class<?>> convert(Stream<Type> typeChain) {
		return typeChain.map(TypeUtils::getClass).collect(Collectors.toList());
	}

	@Test
	void testGetTypeChain() {
		{
			// Classes.
			Assertions.assertEquals(Arrays.asList(ArrayList.class, AbstractList.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, AbstractList.class)));
			Assertions.assertEquals(Collections.singletonList(ArrayList.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, ArrayList.class)));
		}
		{
			// Interfaces.
			Assertions.assertEquals(Arrays.asList(ArrayList.class, List.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, List.class)));
			Assertions.assertEquals(Arrays.asList(ArrayList.class, List.class, Collection.class, Iterable.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, Iterable.class)));
		}
		{
			// KLongMap and StringVMap are at the same distance.
			// TODO: Find out whether the order is determined. It may be the order of interface enumeration.
			Assertions.assertEquals(Arrays.asList(DiamondGenerics.StringLongMap.class, DiamondGenerics.KLongMap.class, DiamondGenerics.RootMap.class),
					convert(HierarchyUtils.getTypeChain(DiamondGenerics.StringLongMap.class, DiamondGenerics.RootMap.class)));
			// Short way is expected.
			Assertions.assertEquals(Arrays.asList(DiamondGenerics.DeepStringLongMap.class, DiamondGenerics.RootMap.class),
					convert(HierarchyUtils.getTypeChain(DiamondGenerics.DeepStringLongMap.class, DiamondGenerics.RootMap.class)));
		}
	}
}
