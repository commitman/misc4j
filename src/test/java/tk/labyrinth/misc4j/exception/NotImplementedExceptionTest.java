package tk.labyrinth.misc4j.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.misc4j.junit5.ContribAssertions;

class NotImplementedExceptionTest {

	private void notImplementedMethod() {
		throw new NotImplementedException();
	}

	@Test
	void testNoArgsConstructor() {
		ContribAssertions.assertThrows(this::notImplementedMethod).ifPresent(throwable -> {
			ContribAssertions.assertInstanceOf(NotImplementedException.class, throwable);
			Assertions.assertEquals("tk.labyrinth.misc4j.exception.NotImplementedExceptionTest.notImplementedMethod(NotImplementedExceptionTest.java:10)",
					throwable.getMessage());
		});
		ContribAssertions.assertThrows(() -> {
			throw new NotImplementedException();
		}).ifPresent(throwable -> {
			ContribAssertions.assertInstanceOf(NotImplementedException.class, throwable);
			Assertions.assertEquals("tk.labyrinth.misc4j.exception.NotImplementedExceptionTest.lambda$testNoArgsConstructor$1(NotImplementedExceptionTest.java:21)",
					throwable.getMessage());
		});
		ContribAssertions.assertThrows(() -> {
			//noinspection TrivialFunctionalExpressionUsage
			((Runnable) () -> {
				throw new NotImplementedException();
			}).run();
		}).ifPresent(throwable -> {
			ContribAssertions.assertInstanceOf(NotImplementedException.class, throwable);
			Assertions.assertEquals("tk.labyrinth.misc4j.exception.NotImplementedExceptionTest.lambda$null$3(NotImplementedExceptionTest.java:30)",
					throwable.getMessage());
		});
	}
}
