package tk.labyrinth.misc4j.reflection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class HierarchyUtilsTest {

	private List<Class<?>> convert(TypeChain typeChain) {
		return typeChain.getTypes().map(TypeUtils::getClass).collect(Collectors.toList());
	}

	@Test
	void testGetTypeChain() {
		{
			// Classes.
			Assertions.assertEquals(Arrays.asList(ArrayList.class, AbstractList.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, AbstractList.class)));
			Assertions.assertEquals(Collections.singletonList(ArrayList.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, ArrayList.class)));
		}
		{
			// Interfaces.
			Assertions.assertEquals(Arrays.asList(ArrayList.class, List.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, List.class)));
			Assertions.assertEquals(Arrays.asList(ArrayList.class, List.class, Collection.class, Iterable.class),
					convert(HierarchyUtils.getTypeChain(ArrayList.class, Iterable.class)));
		}
		{
			// KLongMap and StringVMap are at the same distance.
			// TODO: Find out whether the order is determined. It may be the order of interface enumeration.
			Assertions.assertEquals(Arrays.asList(DiamondGenerics.StringLongMap.class, DiamondGenerics.KLongMap.class, DiamondGenerics.RootMap.class),
					convert(HierarchyUtils.getTypeChain(DiamondGenerics.StringLongMap.class, DiamondGenerics.RootMap.class)));
			// Short way is expected.
			Assertions.assertEquals(Arrays.asList(DiamondGenerics.DeepStringLongMap.class, DiamondGenerics.RootMap.class),
					convert(HierarchyUtils.getTypeChain(DiamondGenerics.DeepStringLongMap.class, DiamondGenerics.RootMap.class)));
		}
	}
}
