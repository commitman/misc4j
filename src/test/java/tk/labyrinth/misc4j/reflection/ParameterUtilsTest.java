package tk.labyrinth.misc4j.reflection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ParameterUtilsTest {

	@Nullable
	private final Map<String, Long> rawField0 = null;

	@Nullable
	private final Map<?, List<Long>> rawField1 = null;

	@Nullable
	private final DiamondGenerics.StringLongMap typedField = null;

	private List<String> convert(Stream<Type> types) {
		return types.map(type -> type != null ? type.getTypeName() : null).collect(Collectors.toList());
	}

	@Test
	void testGetActualParametersForField() throws NoSuchFieldException {
		{
			// Raw fields.
			Assertions.assertEquals(Arrays.asList("java.lang.String", "java.lang.Long"),
					convert(ParameterUtils.getActualParameters(getClass().getDeclaredField("rawField0"), Map.class)));
			Assertions.assertEquals(Arrays.asList("?", "java.util.List<java.lang.Long>"),
					convert(ParameterUtils.getActualParameters(getClass().getDeclaredField("rawField1"), Map.class)));
		}
		{
			// Typed field.
			Assertions.assertEquals(Arrays.asList("java.lang.String", "java.lang.Long"),
					convert(ParameterUtils.getActualParameters(getClass().getDeclaredField("typedField"), Map.class)));
		}
	}

	@Test
	void testGetActualParametersForType() {
		{
			Assertions.assertEquals(Arrays.asList("java.lang.String", "java.lang.Long"),
					convert(ParameterUtils.getActualParameters(DiamondGenerics.StringLongMap.class, Map.class)));
			Assertions.assertEquals(Arrays.asList("java.lang.String", "java.lang.Long"),
					convert(ParameterUtils.getActualParameters(DiamondGenerics.StringLongMap.class, DiamondGenerics.RootMap.class)));
			Assertions.assertEquals(Collections.singletonList("java.lang.String"),
					convert(ParameterUtils.getActualParameters(DiamondGenerics.StringLongMap.class, DiamondGenerics.KLongMap.class)));
			Assertions.assertEquals(Collections.singletonList("java.lang.Long"),
					convert(ParameterUtils.getActualParameters(DiamondGenerics.StringLongMap.class, DiamondGenerics.StringVMap.class)));
		}
		{
			Assertions.assertEquals(Arrays.asList("T", "java.lang.Long"),
					convert(ParameterUtils.getActualParameters(DiamondGenerics.KLongMap.class, Map.class)));
			Assertions.assertEquals(Arrays.asList("java.lang.String", "T"),
					convert(ParameterUtils.getActualParameters(DiamondGenerics.StringVMap.class, Map.class)));
			Assertions.assertEquals(Arrays.asList("A", "B"),
					convert(ParameterUtils.getActualParameters(DiamondGenerics.RootMap.class, Map.class)));
		}
	}
}
