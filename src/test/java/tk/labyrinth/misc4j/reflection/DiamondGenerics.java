package tk.labyrinth.misc4j.reflection;

import java.util.Map;

public class DiamondGenerics {

	public interface DeepStringLongMap extends KLongMap<String>, RootMap<String, Long>, StringVMap<Long> {
		// empty
	}

	public interface KLongMap<T> extends RootMap<T, Long> {
		// empty
	}

	public interface RootMap<A, B> extends Map<A, B> {
		// empty
	}

	public interface StringLongMap extends KLongMap<String>, StringVMap<Long> {
		// empty
	}

	public interface StringVMap<T> extends RootMap<String, T> {
		// empty
	}
}
